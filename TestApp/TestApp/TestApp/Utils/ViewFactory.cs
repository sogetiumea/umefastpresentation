﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace TestApp
{
    public class ViewFactory
    {
		public static void Initialize()
		{
			ViewFactory.Instance = new ViewFactory();
		}

        public void Register<TViewModel, TView>()
            where TViewModel : BaseViewModel
            where TView : Page, new()
        {
            map[typeof(TViewModel)] = typeof(TView);
        }

        public Page Resolve<TViewModel>() where TViewModel : BaseViewModel, new()
        {
            var viewType = map[typeof(TViewModel)];
            var page = (Page)Activator.CreateInstance(viewType);
            var viewModel = Activator.CreateInstance<TViewModel>();

            viewModel.Navigation = page.Navigation;
            page.BindingContext = viewModel;

            return page;
        }

        public Page Resolve<TViewModel>(TViewModel viewModel) where TViewModel : BaseViewModel
        {
            var viewType = map[typeof(TViewModel)];
            var page = (Page)Activator.CreateInstance(viewType);

            viewModel.Navigation = page.Navigation;
            page.BindingContext = viewModel;

            return page;
        }
			
        public static ViewFactory Instance;
		private readonly IDictionary<Type, Type> map = new Dictionary<Type, Type>();

    }
}
