﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestApp.Core;
using Xamarin.Forms;

namespace TestApp
{
    public class App : Application
    {
        public App()
        {
            // The root page of your application
            //MainPage = new ContentPage
            //{
            //    Content = new StackLayout
            //    {
            //        VerticalOptions = LayoutOptions.Center,
            //        Children = {
            //            new Label {
            //                HorizontalTextAlignment = TextAlignment.Center,
            //                Text = "Welcome to Xamarin Forms!"
            //            }
            //        }
            //    }
            //};

            //MainPage = new CodeTestPage();

            RegisterViews();

            //var page = new TestPage();
            //var viewModel = new TestViewModel();
            //page.BindingContext = viewModel;
            //viewModel.Navigation = page.Navigation;

            var page = ViewFactory.Instance.Resolve<TestViewModel>();
            MainPage = new NavigationPage(page);
        }

        public void RegisterViews()
        {
            ViewFactory.Initialize();

            ViewFactory.Instance.Register<TestViewModel, TestPage>();
            ViewFactory.Instance.Register<AnotherViewModel, AnotherPage>();
        }

        public static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<MyMessageManager>().As<IMessageManager>();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
