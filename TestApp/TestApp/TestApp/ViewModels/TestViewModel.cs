﻿
using System.Threading.Tasks;
using System.Windows.Input;
using TestApp.Core;
using Xamarin.Forms;
using XLabs.Ioc;

namespace TestApp
{
    public class TestViewModel : BaseViewModel
    {
        public TestViewModel()
        {
            //HelloMessage = "Hello from ViewModel!";
            var messageManager = Resolver.Resolve<IMessageManager>();
            HelloMessage = messageManager.GetMessage();
        }

        private async Task ButtonTapped()
        {
            //HelloMessage = "Button was tapped!";

            //var page = new AnotherPage();
            //var viewModel = new AnotherViewModel();
            //viewModel.AnotherMessage = "This is another message!";
            //page.BindingContext = viewModel;
            //viewModel.Navigation = page.Navigation;

            var viewModel = new AnotherViewModel();
            viewModel.AnotherMessage = "This is another message!";
            var page = ViewFactory.Instance.Resolve<AnotherViewModel>(viewModel);

            await Navigation.PushAsync(page);
        }

        public string HelloMessage { get; set; }

        private ICommand buttonTappedCommand;
        public ICommand ButtonTappedCommand
        {
            get { return buttonTappedCommand ?? (buttonTappedCommand = new Command(async () => await ButtonTapped())); }
        }
    }
}
