﻿using PropertyChanged;
using Xamarin.Forms;

namespace TestApp
{
    [ImplementPropertyChanged]
    public class BaseViewModel
    {
        public INavigation Navigation { get; set; }

    }
}
