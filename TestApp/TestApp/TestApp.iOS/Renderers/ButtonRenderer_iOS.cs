﻿using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;

[assembly: ExportRenderer (typeof(Button), typeof(TestApp.iOS.ButtonRenderer_iOS))]
namespace TestApp.iOS
{
	public class ButtonRenderer_iOS : ButtonRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged (e);

			if (e.NewElement != null && Control != null) 
			{
				Control.BackgroundColor = UIColor.FromRGB(240, 240, 240);
			}
		}
	}
}

