﻿using System;
using System.Collections.Generic;
using System.Text;
using TestApp.Core;

namespace TestApp.iOS
{
    public class LocalMessageHelper_iOS : ILocalMessageHelper
    {
        public string GetLocalmessage()
        {
            return "Hello from iOS!";
        }
    }
}
