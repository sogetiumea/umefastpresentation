﻿
using Autofac;
using TestApp.Core;
using XLabs.Ioc;
using XLabs.Ioc.Autofac;

namespace TestApp.iOS
{
    public class Bootstrapper_iOS
    {
        public static void Initialize()
        {
            ContainerBuilder builder = new ContainerBuilder();

            // Register common types
            App.RegisterTypes(builder);

            // Register device specific types
            RegisterTypes(builder);

            // Set the Resolver
            AutofacResolver resolver = new AutofacResolver(builder.Build());
            Resolver.SetResolver(resolver);
        }

        public static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<LocalMessageHelper_iOS>().As<ILocalMessageHelper>();
        }
    }
}
