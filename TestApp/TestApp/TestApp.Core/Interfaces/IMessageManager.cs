﻿
namespace TestApp.Core
{
    public interface IMessageManager
    {
        string GetMessage();
    }
}
