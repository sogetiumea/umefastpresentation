﻿
using System;

namespace TestApp.Core
{
    public class MyMessageManager : IMessageManager
    {
        public MyMessageManager(ILocalMessageHelper localMessageHelper)
        {
            this.localMessageHelper = localMessageHelper;
        }

        public string GetMessage()
        {
            return localMessageHelper.GetLocalmessage();
        }

        private ILocalMessageHelper localMessageHelper;
    }
}
