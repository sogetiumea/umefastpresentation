﻿using System;
using System.Collections.Generic;
using System.Text;
using TestApp.Core;

namespace TestApp.Droid
{
    public class LocalMessageHelper_Droid : ILocalMessageHelper
    {
        public string GetLocalmessage()
        {
            return "Hello from Android!";
        }
    }
}
